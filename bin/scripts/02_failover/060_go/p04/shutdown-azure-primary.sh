#!/usr/bin/env bash

set -euo pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
UNSYMLINKED_SCRIPT_DIR="$(readlink -f "${SCRIPT_DIR}" || readlink "${SCRIPT_DIR}" || echo "${SCRIPT_DIR}")"
# shellcheck disable=SC1091,SC1090
source "${UNSYMLINKED_SCRIPT_DIR}/../../../../workflow-script-commons.sh"

if [[ -z $POSTGRESQL_AZURE_PRIMARY ]]; then
  echo "You must set POSTGRESQL_AZURE_PRIMARY in source_vars"
fi

recovery=$(ssh_host "$POSTGRESQL_AZURE_PRIMARY" "sudo gitlab-psql  -t -d gitlab_repmgr -c 'select pg_is_in_recovery();'")
echo "$POSTGRESQL_AZURE_PRIMARY: pg_is_in_recovery=$recovery"

echo "postgresql will be shutdown on the above host, press enter to continue"
read -r

ssh_host "$POSTGRESQL_AZURE_PRIMARY" "sudo gitlab-ctl stop postgresql"

echo "Waiting an appropriate time..."
sleep 5

echo "Getting status:"
p_status=$(ssh_host "$POSTGRESQL_AZURE_PRIMARY" "sudo gitlab-ctl status postgresql")
echo "$POSTGRESQL_AZURE_PRIMARY: $p_status"




